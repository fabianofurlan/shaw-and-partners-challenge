import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  button {
    cursor: pointer;
  }

  html, body, #root {
    height: 100%;
  }

  body {
    text-rendering: optimizeLegibility !important;
    -webkit-font-smoothing: antialised !important;
    background: #282C34;
    font-family: sans-serif;
    color: #FFF;
  }
  a {
    text-decoration: none;
  }
  ul {
    list-style-type: none;
  }
`;

export default GlobalStyle;
