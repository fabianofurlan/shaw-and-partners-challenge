import styled, { css } from 'styled-components';

import { Link } from 'react-router-dom';

export const Container = styled.div`
  display: flex;
  padding: 50px;
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;

  ${props => props.loading
    && css`
      height: 100%;
      display: flex;
      justify-content: center;
      align-items: center;
    `}
`;

export const Card = styled(Link)`
  padding: 15px;
  color: #fff;
  width: 200px;
  border: 1px solid #ccc;
  background-color: #fafafa;
  border-radius: 12px;
  color: #686868;
  margin: 20px;
  cursor: pointer;
  h1 {
    font-size: 25px;
  }
`;

export const Pagination = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  padding: 50px;
`;

export const Button = styled(Link)`
  width: 200px;
  height: 100px;
  background-color: #768d87;
  -moz-border-radius: 5px;
  -webkit-border-radius: 5px;
  border-radius: 5px;
  border: 1px solid #566963;
  display: inline-block;
  cursor: pointer;
  color: #ffffff;
  font-family: Arial;
  font-size: 40px;
  line-height: 75px;
  padding: 11px 23px;
  text-decoration: none;
  text-align: center;
  &:last-child {
    margin-left: auto;
  }
`;
