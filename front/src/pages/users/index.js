import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import queryString from 'query-string';

import { bindActionCreators } from 'redux';
import {
  Container, Card, Pagination, Button,
} from './styles';

import { Creators as UsersActions } from '../../store/ducks/users';

import Loading from '../../components/loading';

export class Users extends Component {
  static propTypes = {
    users: PropTypes.shape({
      data: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.number,
          login: PropTypes.string,
        }),
      ),
      next: PropTypes.number,
      previous: PropTypes.number,
      loading: PropTypes.bool,
    }).isRequired,
    getUsersRequest: PropTypes.func.isRequired,
  };

  componentDidMount() {
    this.loadUsers();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.location.search !== this.props.location.search) {
      this.loadUsers();
    }
  }

  handleBack() {
    window.history.go(-1);
  }

  loadUsers() {
    const { since } = queryString.parse(this.props.location.search);

    this.props.getUsersRequest(since);
  }

  render() {
    const {
      data, previous, next, loading,
    } = this.props.users;
    return (
      <Fragment>
        <Container loading>
          {loading ? (
            <Loading />
          ) : (
            data.map(user => (
              <Card key={user.id} to={`/users/${user.login}`}>
                <h1>
                  <strong>{user.login}</strong>
                </h1>
                <h4>
                  ID:
                  <strong>{user.id}</strong>
                </h4>
              </Card>
            ))
          )}
        </Container>

        {!loading && (
          <Pagination>
            {previous && <Button onClick={this.handleBack}>Previous</Button>}

            {next && <Button to={`/?since=${next}`}>Next</Button>}
          </Pagination>
        )}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  users: state.users,
});

const mapDispatchToProps = dispatch => bindActionCreators(UsersActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Users);
