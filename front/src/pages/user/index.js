import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { bindActionCreators } from 'redux';
import {
  Container, Header, RepositoryItem, RepositoryList,
} from './styles';
import Loading from '../../components/loading';

import { Creators as UserDetailsActions } from '../../store/ducks/userDetails';

export class User extends Component {
  static propTypes = {
    userDetails: PropTypes.shape({
      data: PropTypes.shape({
        id: PropTypes.number,
        avatar_url: PropTypes.string,
        login: PropTypes.string,
        created_at: PropTypes.string,
        html_url: PropTypes.string,
        repositories: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.number,
            name: PropTypes.string,
            html_url: PropTypes.string,
          }),
        ),
      }),
      loading: PropTypes.bool,
    }).isRequired,
    getUserDetailsRequest: PropTypes.func.isRequired,
  };

  componentDidMount() {
    this.loadUserDetails();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.username !== this.props.match.params.username) {
      this.loadUserDetails();
    }
  }

  loadUserDetails = () => {
    const { username } = this.props.match.params;

    this.props.getUserDetailsRequest(username);
  };

  renderDetails = () => {
    const user = this.props.userDetails.data;
    return (
      <Container>
        <Header>
          <img src={user.avatar_url} alt={user.login} />
          <div>
            <span>{user.id}</span>
            <h1>{user.login}</h1>
            <p>{user.created_at}</p>

            <a href={user.html_url} target="_blank" type="button">
              Profile URL
            </a>
          </div>
        </Header>
        <RepositoryList cellPadding="0" cellSpacing="0">
          <thead>
            <tr>
              <th />
              <th>Id</th>
              <th>Name</th>
              <th>Url</th>
            </tr>
          </thead>
          <tbody>
            {!user.repositories ? (
              <tr>
                <td colSpan="5">Nenhum repositorio cadastrado</td>
              </tr>
            ) : (
              user.repositories.map(repository => (
                <RepositoryItem key={repository.id}>
                  <td />
                  <td>{repository.id}</td>
                  <td>{repository.name}</td>
                  <td>{repository.html_url}</td>
                </RepositoryItem>
              ))
            )}
          </tbody>
        </RepositoryList>
      </Container>
    );
  };

  render() {
    return this.props.userDetails.loading ? (
      <Container loading>
        <Loading />
      </Container>
    ) : (
      this.renderDetails()
    );
  }
}

const mapStateToProps = state => ({
  userDetails: state.userDetails,
});

const mapDispatchToProps = dispatch => bindActionCreators(UserDetailsActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(User);
