import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 50px;
  flex-wrap: wrap;
`;

export const Header = styled.header`
  display: flex;
  align-items: center;

  img {
    width: 220px;
    height: 220px;
  }

  div {
    margin-left: 20px;
    span {
      font-size: 11px;
      color: #fff;
      letter-spacing: 1.11px;
      font-weight: 300;
    }

    h1 {
      margin-top: 10px;
      font-size: 48px;
    }

    p {
      margin-top: 0;
      color: #b3b3b3;
      font-size: 11px;
      letter-spacing: 1.11px;
      text-transform: uppercase;
    }

    a {
      background: #1db854;
      height: 32px;
      border-radius: 3px;
      color: #fff;
      line-height: 32px;
      padding: 5px 35px;
      border: 0;
      margin-top: 10px;
      font-size: 12px;
      letter-spacing: 1.11px;

      &:hover {
        opacity: 0.8;
      }
    }
  }
`;

export const RepositoryList = styled.table`
  width: 100%;
  text-align: left;
  margin-top: 20px;

  thead th {
    font-size: 11px;
    color: #b3b3b3;
    letter-spacing: 1.11px;
    font-weight: normal;
    text-transform: uppercase;
    padding: 5px 10px;
  }
`;

export const RepositoryItem = styled.tr`
  td {
    border-top: 1px solid #282828;
    font-size: 13px;
    padding: 0 10px;
    line-height: 40px;
    background: ${props => (props.selected ? '#282828' : 'transparent')}
    color: ${props => (props.playing ? '#1ed760' : '#fff')}

    &:first-child {
      width: 80px;
      text-align: right;
    }
  }
  &:hover td {
    background: #282828;
  }
`;
