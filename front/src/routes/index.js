import React, { Fragment } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import User from '../pages/user';
import Users from '../pages/users';

const Routes = () => (
  <BrowserRouter>
    <Fragment>
      <Switch>
        <Route exact path="/" component={Users} />
        <Route path="/users/:username" component={User} />
      </Switch>
    </Fragment>
  </BrowserRouter>
);

export default Routes;
