import { all, takeLatest } from 'redux-saga/effects';

import { Types as UsersTypes } from '../ducks/users';
import { Types as UserDetailsTypes } from '../ducks/userDetails';

import { getUsers } from './users';
import { getUserDetails } from './userDetails';

export default function* rootSaga() {
  yield all([takeLatest(UsersTypes.GET_REQUEST, getUsers)]);
  yield all([takeLatest(UserDetailsTypes.GET_REQUEST, getUserDetails)]);
}
