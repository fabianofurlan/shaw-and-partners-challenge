import { call, put } from 'redux-saga/effects';
import api from '../../services/api';

import { Creators as UsersActions } from '../ducks/users';
import { Creators as ErrorActions } from '../ducks/error';

export function* getUsers(action) {
  try {
    const response = yield call(api.get, `/users?since=${action.payload.since}`);
    yield put(UsersActions.getUsersSuccess(response.data, action.payload.since));
  } catch (error) {
    yield put(ErrorActions.setError('Não foi possivel obter os usuários'));
  }
}
