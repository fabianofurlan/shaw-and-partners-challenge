import { call, put } from 'redux-saga/effects';
import api from '../../services/api';

import { Creators as UserDetailsActions } from '../ducks/userDetails';
import { Creators as ErrorActions } from '../ducks/error';

export function* getUserDetails(action) {
  try {
    const response = yield call(api.get, `/users/${action.payload.username}`);

    yield put(UserDetailsActions.getUserDetailsSuccess(response.data));
  } catch (error) {
    yield put(
      ErrorActions.setError(`Não foi possivel obter o usuario: ${action.payload.username}`),
    );
  }
}
