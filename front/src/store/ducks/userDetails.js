/* eslint-disable no-case-declarations */
/**
 * Types
 */

export const Types = {
  GET_REQUEST: 'userDetails/GET_REQUEST',
  GET_SUCCESS: 'userDetails/GET_SUCCESS',
};

const INITIAL_STATE = {
  loading: false,
  data: {},
};

export default function userDetails(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.GET_REQUEST:
      return { ...state, loading: true };
    case Types.GET_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload.data,
      };
    default:
      return { ...state };
  }
}

/**
 * Creators
 */

export const Creators = {
  getUserDetailsRequest: username => ({ type: Types.GET_REQUEST, payload: { username } }),
  getUserDetailsSuccess: data => ({ type: Types.GET_SUCCESS, payload: { data } }),
};
