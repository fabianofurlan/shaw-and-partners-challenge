import { combineReducers } from 'redux';
import users from './users';
import userDetails from './userDetails';
import error from './error';

export default combineReducers({
  users,
  userDetails,
  error,
});
