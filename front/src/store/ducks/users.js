/* eslint-disable no-case-declarations */
/**
 * Types
 */

export const Types = {
  GET_REQUEST: 'users/GET_REQUEST',
  GET_SUCCESS: 'users/GET_SUCCESS',
};

const INITIAL_STATE = {
  loading: false,
  data: [],
  previous: null,
  next: null,
  since: null,
};

export default function users(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.GET_REQUEST:
      return { ...state, loading: true };
    case Types.GET_SUCCESS:
      // eslint-disable-next-line no-case-declarations
      const lastItem = action.payload.data.slice(-1)[0];
      const firstItem = action.payload.data[0];

      let previous = null;
      const next = lastItem.id;
      if (action.payload.since) {
        previous = firstItem.id - 1;
      }

      return {
        ...state,
        loading: false,
        data: action.payload.data,
        next,
        previous,
      };
    default:
      return { ...state };
  }
}

/**
 * Creators
 */

export const Creators = {
  getUsersRequest: since => ({ type: Types.GET_REQUEST, payload: { since } }),
  getUsersSuccess: (data, since) => ({ type: Types.GET_SUCCESS, payload: { data, since } }),
};
