import React, { Fragment } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import GlobalStyle from './styles/global';
import store from './store';

import Routes from './routes';

import ErrorBox from './components/errorBox';

const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <Fragment>
        <GlobalStyle />
        <ErrorBox />
        <Routes />
      </Fragment>
    </BrowserRouter>
  </Provider>
);

export default App;
