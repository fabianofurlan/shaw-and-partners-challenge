require('dotenv').config()

const express = require('express')
const Sentry = require('@sentry/node')
const sentryConfig = require('./config/sentry')
const cors = require('cors')

class App {
  constructor () {
    this.express = express()
    this.isDev = process.env.NODE_ENV !== 'production'

    this.database()
    this.middlewares()
    this.sentry()
    this.routes()
    this.exception()
  }

  sentry () {
    Sentry.init(sentryConfig)
  }

  database () {}

  middlewares () {
    this.express.use(cors())
  }

  routes () {
    if (process.env.NODE_ENV === 'production') {
      this.express.use(Sentry.Handlers.requestHandler())
    }
    this.express.use(require('./routes'))
  }

  exception () {
    if (process.env.NODE_ENV === 'production') {
      this.express.use(Sentry.Handlers.errorHandler())
    }
  }
}

module.exports = new App().express
