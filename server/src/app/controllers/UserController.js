const githubAPI = require('../services/github.js')

class UserController {
  async index (req, res) {
    try {
      const since = parseInt(req.query.since)
      const response = await githubAPI.get(`/users?since=${since}`)

      res.send(response.data)
    } catch (error) {
      res.status(error.response.status).send(error.response.data.message)
    }
  }

  async show (req, res) {
    try {
      const { username } = req.params
      const user = await githubAPI.get(`/users/${username}`)
      const repositories = await githubAPI.get(`/users/${username}/repos`)

      const response = { ...user.data, repositories: repositories.data }

      res.send(response)
    } catch (error) {
      res.status(error.response.status).send(error.response.data.message)
    }
  }
}

module.exports = new UserController()
