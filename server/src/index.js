const server = require('./server')

server.listen(process.env.PORT || 3001, function () {
  console.log('Running on port: ' + process.env.PORT || 3001)
})
